//Created by Rehan - 081295955149
package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("---------------------")
	fmt.Print("N M-> ")
	input1, _ := reader.ReadString('\n')
	input2 := strings.Split(input1, " ")
	n, _ := strconv.Atoi(input2[0])
	var m int
	if _, err := fmt.Sscan(input2[1], &m); err != nil {
		fmt.Println(err)
	}
	newArray := make([]int, n)
	for i := 0; i < m; i++ {
		fmt.Print("a b k-> ")
		otherInput, _ := reader.ReadString('\n')
		splitting := strings.Split(otherInput, " ")
		first, _ := strconv.Atoi(splitting[0])
		second, _ := strconv.Atoi(splitting[1])
		var third int
		if _, err := fmt.Sscan(splitting[2], &third); err != nil {
			fmt.Println(err)
		}
		newArray[first-1] = newArray[first-1] + third
		newArray[second-1] = newArray[second-1] + third
	}
	fmt.Println("---------------------")
	sort.Sort(sort.Reverse(sort.IntSlice(newArray)))
	fmt.Println("Output => ", newArray[0])
}
